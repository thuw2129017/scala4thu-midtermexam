import scala.util.Random

/**
  * Created by mark on 08/04/2017.
  */
object Questions {
  /** 5%
    * 請用Tail Recursive寫出階乘函數fac(n)=n*(n-1)*...*2*1
    * ex:
    * fib(5)=120
    **/
  def tailFac(n:Int,acc:Int):Int= {
    if(n==0) acc
    else{
      tailFac(n-1,n*acc)
    }

  }

  /** 5%
    * 請用Recursive方式找出List中最大數字
    * ex:
    * input:  List(1, 5, 7, 2, 1)
    * output: 7
    **/
  def maxRecur(list:List[Int]):Int={
    list.foldLeft(0)((acc,x)=> Math.max(acc,x))
  }

  /** 10%
    * 請用Recursive反轉List
    * ex:
    * input:  List(1.1, 2.2, 3.3)
    * output: List(3.3, 2.2, 1.1)
    **/
  def reverseRecur(list:List[Double]):List[Double]= {
    list.foldLeft(List(0.0))((acc,x)=> x +: acc)
  }

  /** 30%
    * 計算List中每個字串出現的次數
    * ex:
    * input:  List(Apple, Banana, Apple, Cherry, Cherry, Apple)
    * output: Map(Apple->3, Banana->1, Cherry->2)
    **/
  def wordCount(words:List[String])= {
    words.groupBy(w => w).mapValues(_.length)
  }

  /** 30%
    * 計算List中每個數字由左至右的累加值
    * ex:
    * input:  List(1, 2, 3, 4, 5)
    * output: List(1, 3, 6, 10, 15)
    **/
  def accumulator(nums:List[Int])={
      nums.scan(0)(_+_).tail
  }

  /** 20%
    * 有編號1到1000的球，請寫出一個以取後不放回方式隨機取出n顆球的函數
    * (取出n顆球的順序是隨機的並且不能重複)
    * ex:
    input:  3
    output: List(65, 3, 134) 結果為隨機的
    input:  5
    output: List(45, 768, 476, 92, 134) 結果為隨機的
    **/
  import scala.util.Random
  def selectWithoutReplace(n:Int):List[Int]={
    val balls=(1 to 1000).toList
    var result = List()

    for(i<- 1 to n) {
      result.+:(balls(Random.nextInt(balls.size)))
    }


  }
  /** 20%
    * 請將巢狀集合攤平成集合
    * ex:
    input:  List(List(1),List(2,2),List(3,3,3))
    output: List(1,2,2,3,3,3)
    提示:
    你可以考慮使用List本身提供的flatMap或者是:::這兩種方法
    當然也可以發揮你的創意，想想看其他作法。
    **/
  def flatten(nestedList:List[List[Int]]):List[Int]= {
    nestedList.flatten

  }

}
